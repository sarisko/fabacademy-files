use <MCAD/boxes.scad>

$fa = 1;
$fs = 0.4;

// the overlapping constant
c = 0.001;

// parameters
wheel_size1 = 4;
wheel_size2 = 6;
base_x = 38;
base_y = 15;
base_z = 7;

motor_x = base_x - 2;
motor_y = base_y - 2;
motor_z = 8;

driver_space_x = 15;
driver_space_y = driver_space_x;
driver_space_z = 17;

roof_x = 16; 
roof_y = 16;
roof_z = 2;

chimney_from_edge = 5;
chimney_thin_size = 1.5;
chimney_thick_size = 3.5;
chimney_height = 8;

base_wheel_space = 1.5;
wheel_rod_radius = 1;

wheel1_position_x = -12.5;
wheel2_position_x = -3.5;
wheel3_position_x = 10;

wheel_small_mid_z = 2.5;
wheel_big_mid_z = 4.5;

wheel_thickness = 1.5;
wheel_small_r = 4;
wheel_big_r = 6;
wheel_y = base_y/2+base_wheel_space - c;

window_x = 5.5;
window_y = 0.5;
window_z = window_x;

// base
translate([0,0,base_z/2])
    cube([base_x,base_y,base_z],center=true);
// motor
translate([0,0,base_z+motor_z/2])
    cube([motor_x,motor_y,motor_z+c],center=true);
// driver space + windows
translate([base_x/2-driver_space_x/2,0,base_z+driver_space_z/2])
    difference(){
        cube([driver_space_x,driver_space_y,driver_space_z],center=true);
        translate([0,driver_space_y/2-window_y/2,3])
            roundedBox(size=[window_x,window_y+0.1,window_z],radius=0.2, sidesonly=false);
        translate([0,-(driver_space_y/2-window_y/2),3])
            roundedBox(size=[window_x,window_y+0.1,window_z],radius=0.2, sidesonly=false);
    }
// roof
translate([base_x/2-driver_space_x/2,0,base_z+driver_space_z-c])
    roundedBox(size=[roof_x,roof_y,roof_z],radius=1, sidesonly=false);
// chimney 1
translate([-(motor_x/2-chimney_from_edge),0,base_z+motor_z])
    cylinder(h=chimney_height/2+2*c,r=chimney_thin_size);
// chimney 2
translate([-(motor_x/2-chimney_from_edge),0,base_z+motor_z+chimney_height/2])
    cylinder(h=chimney_height/2,r1=chimney_thin_size, r2=chimney_thick_size);
// support for wheels
translate([wheel1_position_x,0,wheel_small_mid_z])
    rotate([90,0,0])
    cylinder(h=base_y+base_wheel_space*2,r=wheel_rod_radius,center=true);
translate([wheel2_position_x,0,wheel_small_mid_z])
    rotate([90,0,0])
    cylinder(h=base_y+base_wheel_space*2,r=wheel_rod_radius,center=true);
translate([wheel3_position_x,0,wheel_big_mid_z])
    rotate([90,0,0])
    cylinder(h=base_y+base_wheel_space*2,r=wheel_rod_radius,center=true);   
// wheels
translate([wheel1_position_x,wheel_y,wheel_small_mid_z])
    rotate([90,0,0])
    cylinder(h=wheel_thickness,r=wheel_small_r,center=true);
translate([wheel1_position_x,-(wheel_y),wheel_small_mid_z])
    rotate([90,0,0])
    cylinder(h=wheel_thickness,r=wheel_small_r,center=true);
translate([wheel2_position_x,wheel_y,wheel_small_mid_z])
    rotate([90,0,0])
    cylinder(h=wheel_thickness,r=wheel_small_r,center=true);
translate([wheel2_position_x,-(wheel_y),wheel_small_mid_z])
    rotate([90,0,0])
    cylinder(h=wheel_thickness,r=wheel_small_r,center=true);
translate([wheel3_position_x,wheel_y,wheel_big_mid_z])
    rotate([90,0,0])
    cylinder(h=wheel_thickness,r=wheel_big_r,center=true);
translate([wheel3_position_x,-(wheel_y),wheel_big_mid_z])
    rotate([90,0,0])
    cylinder(h=wheel_thickness,r=wheel_big_r,center=true);